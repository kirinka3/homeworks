"use strict";

const tabsTitle = document.querySelectorAll('.tabs-title');
const tabsItems = document.querySelectorAll('.tabs-item');

tabsTitle.forEach(onTabClick);

function onTabClick(item) {
    item.addEventListener('click', function() {
        let currentTabsTitle = item;
        let tabId = currentTabsTitle.getAttribute("data-tab");
        let currentTab = document.querySelector(tabId);

        if( ! currentTabsTitle.classList.contains('active') ) {
            tabsTitle.forEach(function(item) {
                item.classList.remove('active')
            });

            tabsItems.forEach(function(item) {
                item.classList.remove('active')
        });

            currentTabsTitle.classList.add('active');
            currentTab.classList.add('active');
        }
    });
}
























