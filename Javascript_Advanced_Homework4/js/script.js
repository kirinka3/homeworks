const div = document.createElement('div');
const ul = document.createElement('ul');
document.body.append(div);
div.append(ul);

fetch('https://swapi.dev/api/films/')
    .then(response => response.json())
    .then(data => {
        for ( let key in data.results) {
        ul.innerHTML +=`<li>
            <p>${data.results[key].episode_id}</p>
            <p>${data.results[key].title}</p>
            <p class="name_${key}"></p>
            <p>${data.results[key].opening_crawl}</p>
        </li>`;
        }
        data.results.forEach((film, item) => {
            film.characters.forEach((url) => {
            fetch(url)
                .then(response => response.json())
                .then(data => {
                    const heroes = document.getElementsByClassName(`name_${item}`)[0];
                    heroes.append(data.name + "; ");
                })
            })
        })
    })
