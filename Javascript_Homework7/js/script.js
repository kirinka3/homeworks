"use strict";

const arr = [];

function showList(array, parent=document.body) {
	const ul = document.createElement('ul');
    const newArr = array.map(item => `<li>${item}</li>`);
    ul.innerHTML = newArr.join('');
	parent.append(ul);
}
showList(["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"]);
showList(["1", "2", "3", "sea", "user", 23]);



