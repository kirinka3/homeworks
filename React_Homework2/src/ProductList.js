import React, { Component } from 'react';
import Product from './Product';

class ProductList extends Component {

    addToFavorite = (id) => {
        const favArray = localStorage.getItem('favorite')
        if(favArray) {
            const parsedArr = JSON.parse(favArray)
            const indexFav = parsedArr.indexOf(id)
            if (indexFav === -1) {
                parsedArr.push(id)
                localStorage.setItem('favorite', JSON.stringify(parsedArr))
            } else {
                const spliced = parsedArr.splice(indexFav, 1)
                console.log(spliced);
                localStorage.setItem('favorite',JSON.stringify(parsedArr))
            }
        } else {
            localStorage.setItem('favorite',JSON.stringify([id]))
        }
    }
    
    render() {
        const phonesArr = this.props.phonesList.map((item, index) => {
        return (
            <Product key={item.code}
                code={item.code}
                product={item.product}
                price={item.price}
                color={item.color} 
                url={item.url}
                onClickStar={() => this.addToFavorite(item.code)} 
                />
            )
        })

        return (
            <div className="card_list">{phonesArr}</div>
        )
    }
}

export default ProductList;