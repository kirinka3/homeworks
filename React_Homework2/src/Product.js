import React, { Component } from 'react';
import Modal from './Modal';
import Button from './Button';


class Product extends Component {
  state = {
      modalIsOpened: false,
      starColor: 'black'
  }

  changeColor = () => {
      const { starColor } = this.state
      const { onClickStar } = this.props
      if (starColor === 'black') {
          this.setState ({ starColor: 'yellow'})
      } else if (starColor === 'yellow') {
          this.setState ({ starColor: 'black'})
      }
    onClickStar()
  }

  saveStorage = (id) => {
    const cartArr = localStorage.getItem('cart')
    if(cartArr) {
        const parsedArr = JSON.parse(cartArr)
        const indexCart = parsedArr.indexOf(id)
        if (indexCart === -1) {
            parsedArr.push(id)
            localStorage.setItem('cart', JSON.stringify(parsedArr))
        }
    } else {
        localStorage.setItem('cart',JSON.stringify([id]))
    }  
  }

  showModal = () => {
    this.setState({ modalIsOpened: true })
  }

  closeModal = () => {
    if(this.state.modalIsOpened) {
      this.setState({ modalIsOpened: false })
    }
  }

  render() {
      const { code, product, price, color, url } = this.props;

      return (
        <>
          <div className="card_item">
            <h2 className="card-title">{product}</h2>
            <img src={url} alt="phone" style={{width: '150px', height: '200px'}}/>
            <div className="card_text">Product code: {code}</div>
            <div className="card_text">Product color: {color}</div>
            <div className="card_text">Product price: {price}$</div>
            <div className="rating" onClick={this.changeColor}>
                <label style={{color: this.state.starColor}}>☆☆☆☆☆</label>
            </div>
            <Button text='Add to cart' onClick={this.showModal} />
          </div>
          
            {this.state.modalIsOpened && <Modal 
            header="" 
            text="You have added item to cart" 
            isClose
            actions={ 
            <>
              <Button backgroundColor="grey" text="OK" onClick={this.saveStorage(code)} />
              <Button backgroundColor="grey" text="CLOSE" onClick={this.closeModal} />
            </>}
            closeWindow={this.closeModal}
            />}
        </>    
      )
  }
}

export default Product;