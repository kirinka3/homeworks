import React, { Component } from 'react';
import Main from './Main';
import './App.scss';

class App extends Component {

  render() {
    return (
      <div>
        <Main />
      </div>
    )
  }
}

export default App;
