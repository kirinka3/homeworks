import React, { Component } from 'react';
import ProductList from './ProductList';


class Main extends Component {
    constructor(props) {
        super(props)
        this.state = {
          phones: [],
        }
      }
      
    componentDidMount(){
        fetch('productsList.json')
        .then (response => response.json())
        .then (data => this.setState({phones: data}));
      }

    render() {
        return (
            <>
              <ProductList phonesList={this.state.phones}/>
            </>
        )
    }
}

export default Main;