import React, { Component } from 'react';
import PropTypes from 'prop-types'

class Button extends Component {

    render() {
        const { backgroundColor, text, onClick} = this.props

        return (
            <button className={backgroundColor} onClick={onClick}>{text}</button>
        )
    }
}

Button.propTypes={
    backgroundColor: PropTypes.string,
    text: PropTypes.string.isRequired,
    onClick: PropTypes.func
};

export default Button;