const gulp = require('gulp');
const concat = require('gulp-concat');
const del = require('delete');
const uglify = require('gulp-uglify');
const htmlmin = require('gulp-htmlmin');
const autoprefixer = require('gulp-autoprefixer');
const sass = require('gulp-sass');
const imagemin = require('gulp-imagemin');
const newer = require('gulp-newer');
const cleancss = require('gulp-clean-css');
const rename = require('gulp-rename');
const browserSync = require('browser-sync').create();


function browsersync() {
    browserSync.init({
        server: { baseDir: './'},
        notify: false,
        online: true
    })
}

function buildHTML() {
    return gulp.src('src/*.html')
        .pipe(htmlmin({ collapseWhitespace: true }))
        .pipe(gulp.dest('dist'))
}

function buildCSS() {
    return gulp.src('src/scss/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(concat('style.css'))
        .pipe(autoprefixer({
            cascade: false
        }))
        .pipe(cleancss())
        .pipe(rename({
            extname: '.min.css'
        }))
        .pipe(gulp.dest('dist'))
        .pipe(browserSync.stream())
}

function buildImages() {
    return gulp.src('src/img/*')
        .pipe(newer('dist/img'))
        .pipe(imagemin()) 
        .pipe(gulp.dest('dist/img'))
        .pipe(browserSync.stream())
}

function buildJS() {
    return gulp.src('src/js/*.js')
        .pipe(uglify())
        .pipe(rename({
            extname: '.min.js'
        }))
        .pipe(gulp.dest('dist'))
        .pipe(browserSync.stream())
}

function clean() {
    return del('dist/**', { force: true });
}

function watch() {
    gulp.watch('./index.html').on('change', browserSync.reload)
    gulp.watch('src/**/*', build())
}


function build() {
    return gulp.series([
        clean,
        gulp.parallel([
            buildHTML,
            buildCSS,
            buildImages,
            buildJS
        ])
    ])
}

function dev() {
    return gulp.parallel([
        browsersync,
        watch
    ])
}



exports.browsersync = browsersync;
exports.buildHTML = buildHTML;
exports.buildCSS = buildCSS;
exports.buildJS = buildJS;
exports.buildImages = buildImages;
exports.clean = clean;
exports.watch = watch;

exports.dev = dev();
exports.default = build();

