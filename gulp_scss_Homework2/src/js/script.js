'use strict';

const burgerBtn = document.querySelector('.menu-btn');
const burgerMenu = document.querySelector('.menu_header');
burgerBtn.addEventListener('click', function() {
    this.classList.toggle('open');
    burgerMenu.classList.toggle('active');
})