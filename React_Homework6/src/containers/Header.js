import React from 'react'
import { Link } from 'react-router-dom'

export default function Header() {
    return (
        <div className="container">
            <Link className="navbar-brand" to="/">Home</Link>
            <Link className="navbar-brand" to="/cart">Cart</Link>
            <Link className="navbar-brand" to="/favorites">Favorites</Link>
        </div>
    )
}