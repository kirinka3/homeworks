import Button from './Button'
import { render, fireEvent } from '@testing-library/react'

describe('Button component', () => {
    it('should render correctly', () => {
        const { getByRole } = render(<Button />)
        getByRole('button')
    })

    it('should render correctly with text', () => {
        const text = 'OK';
        const { getByText } = render(<Button text={text} />);
        getByText(text)
    })

    it('should pass click', () => {
        const onClick = jest.fn()
        const {queryByRole} = render(<Button onClick={onClick}>OK</Button>)
        fireEvent(
            queryByRole('button'),
            new MouseEvent('click', { bubbles: true})
        )
        expect(onClick).toHaveBeenCalled()
    })
})