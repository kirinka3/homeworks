import Modal from './Modal'
import { render, fireEvent, screen } from '@testing-library/react'

describe('Modal component', () => {
    it('should render correctly', () => {
        render (<Modal />)
    })

    it('should add class', () => {
        const text = 'text test'
        const {getByText} = render(<Modal text={text}/>)
        getByText(text)
    })

})