import React, { useState, useEffect} from 'react';
import Button from '../components/Button';
import PropTypes from 'prop-types'

const Product = (props) => {
  const [starColor, setStarColor] = useState('black');

  useEffect(() => {
    const articleArr = localStorage.getItem('favorite');
    const parsedArr = JSON.parse(articleArr)

    if (parsedArr) {
        parsedArr.forEach(element => {
            if (element === props.code) {
                setStarColor('yellow')
            }
        })
    } 
  }, [props.code])

  const  changeColor = () => {
      if (starColor === 'black') {
          setStarColor ('yellow')
      } else if (starColor === 'yellow') {
        setStarColor ('black')
      }
    props.onClickFavorite()
  }

  const { code, product, price, color, url, onClickAddToCard } = props;

      return (
        <>
          <div className="card_item">
            <h2 className="card-title">{product}</h2>
            <img src={url} alt="phone" style={{width: '150px', height: '200px'}}/>
            <div className="card_text">Product code: {code}</div>
            <div className="card_text">Product color: {color}</div>
            <div className="card_text">Product price: {price}$</div>
            {
              !props.isInCart ? 
              <div className="rating" onClick={changeColor}>
                <label style={{color: starColor}}>☆</label>
              </div>
              : null
            }
            <Button text={props.isInCart ? 'Delete' : 'Add to cart'} onClick={onClickAddToCard} />
          </div>
        </>    
      )
}


Product.propTypes = {
  code: PropTypes.number,
  product: PropTypes.string,
  price: PropTypes.number,
  color: PropTypes.string,
  url: PropTypes.string,
  changeColor: PropTypes.func,
  onClickAddToCart: PropTypes.func,
};

export default Product;