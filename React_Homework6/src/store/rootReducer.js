import {combineReducers} from 'redux'
import cardReducer from "./card/cardReducer"
import modalReducer from './modal/modalReducer'
import formReducer from './form/formReducer'

export default combineReducers({
    cardReducer,
    modalReducer,
    formReducer
})


