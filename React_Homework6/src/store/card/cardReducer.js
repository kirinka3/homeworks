import * as types from '../types'

const initialState = {
    phones: []
};

const cardReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.SET_CARDS:
            return {
                ...state,
                phones: action.payload
            }
        default: 
        return state
    }
}


export default cardReducer