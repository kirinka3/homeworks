import * as types from '../types'

const initialState = {
    formData: {}
};

const formReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.SET_FORM_DATA:
            return {
                formData: action.payload
            }
        case types.CLEAR_STORAGE:
            localStorage.removeItem('cart')
        default: 
        return state
    }
}

export default formReducer
