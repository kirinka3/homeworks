import {useState, useEffect} from 'react';

const useGetPhones = () => {
    const [phones, setPhones] = useState([]);
    useEffect(() => {
        fetch('productsList.json')
        .then(response=> response.json())
        .then(data => setPhones(data));
    }, [])
    return phones;
}

export default useGetPhones