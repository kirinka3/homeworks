import React, { useState, useEffect } from 'react';
import ProductList from '../components/ProductList';
import { useSelector, useDispatch } from 'react-redux';
import fetchCards from '../store/card/actions';

const Favorites = () => {
    const dispatch = useDispatch()
    const phones = useSelector(store => store.cardReducer.phones)

    useEffect(() => {
        dispatch(fetchCards())
    }, [dispatch])

    const [favoritePhones, setFavoritePhones] = useState([])

    const phoneArr = JSON.parse(localStorage.getItem('favorite'));
    useEffect(() => {
        if (phoneArr) {
            setFavoritePhones(
                phones.filter(obj => phoneArr.find(id => id === obj.code))
        )}
    }, [phoneArr])

    return (
        <div>
            <h1>Favorites : </h1>
            <ProductList phonesList={favoritePhones}/>
        </div>
    )
}

export default Favorites