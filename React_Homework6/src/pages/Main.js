import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux'
import ProductList from '../components/ProductList';
import fetchCards from '../store/card/actions';


const Main = () => {
  const dispatch = useDispatch()
  const phones = useSelector(store => store.cardReducer.phones)

  useEffect(() => {
    dispatch(fetchCards())
  },[dispatch])

  return (
    <ProductList phonesList={phones}/>
  )
}

export default Main;