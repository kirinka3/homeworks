import React, { useState, useEffect } from 'react';
import ProductList from '../components/ProductList';
import { useSelector, useDispatch} from 'react-redux'
import fetchCards from '../store/card/actions';
import CartForm from '../components/Form/CartForm'

const Cart = () => {
  const dispatch = useDispatch()
  const phones = useSelector(store => store.cardReducer.phones)

  useEffect(() => {
    dispatch(fetchCards())
  },[dispatch])

  
  const [cartPhones, setCartPhones] = useState([]);

  const phoneArr = JSON.parse(localStorage.getItem('cart'));
  useEffect(() => {
    if (phoneArr) {
      setCartPhones(
        phones.filter(obj => phoneArr.find(id => id === obj.code))
      )
    }
  }, [phoneArr])

  return (
    <div>
      <h1>Cart : </h1>
      <ProductList isInCart phonesList={cartPhones}/>
      {cartPhones.length > 0 && <CartForm />}
    </div>
  )
}

export default Cart