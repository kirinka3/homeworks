"use strict";

let page = document.querySelector('.page');
let themeButton = document.querySelector('.theme-button');

themeButton.addEventListener('click', function() {
    if (localStorage.getItem('theme')) {
        localStorage.removeItem('theme')
        page.classList.toggle('dark-theme');
} else {
    localStorage.setItem('theme', 'dark');
    page.classList.toggle('dark-theme');
}
});

document.addEventListener("DOMContentLoaded", function (){
    if (localStorage.getItem('theme') !== null) {
        page.classList.toggle('dark-theme');
    }
})












// document.addEventListener("DOMContentLoaded", () => {
//     const savedTheme = localStorage.getItem("theme");

//     applyTheme(savedTheme);

//     for (const optionElement of document.querySelectorAll("#theme option")) {
//         optionElement.selected = savedTheme === optionElement.value;
//     }

//     document.querySelector("#theme").addEventListener("change", function () {
//         localStorage.setItem("theme", this.value);
//         applyTheme(this.value);
//     });
// });



