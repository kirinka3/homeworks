"use strict";

const images = ['1.png', '2.png', '3.png', '4.png'];
const slider = document.querySelector('#slider');
const img = slider.querySelector('img');

let i = 1;
img.src = "img/" + images[0];
let interval;
interval = setInterval(function() {
    img.src = "img/" + images[i];
    i++;
    if (i == images.length) {
        i = 0;
    }
}, 3000);

const btnStop = document.querySelector('#stop');
const btnStart = document.querySelector('#start');

btnStop.addEventListener('click', function() {
    clearInterval(interval);
});

btnStart.addEventListener('click', function() {
    interval = setInterval(function () {
        img.src = "img/" + images[i];
        i++;
        if (i == images.length) {
            i = 0;
        }
    }, 3000);
})
