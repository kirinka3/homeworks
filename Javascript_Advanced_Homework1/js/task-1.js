"use strict";

// Задание 1

const clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];

    //вариант1
    const clients = [...clients1, ...clients2];
    const newArr = []
    clients.forEach(item => {
        if(!newArr.includes (item)) {
            newArr.push(item)
        }
    })
    console.log(newArr);

    //вариант2
    const clientsSet = new Set([...clients1, ...clients2]);
    const newClients = [...clientsSet];
    console.log(newClients);








