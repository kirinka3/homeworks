"use strict";

// Задание 3

const user1 = {
    name: "John",
    years: 30
};

const { name, years: age, isAdmin = false } = user1;

console.log(name, age, isAdmin);