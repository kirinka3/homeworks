"use strict";

// Задание 6

const employee = {
    name: 'Vitalii',
    surname: 'Klichko'
}


const newEmployee = {
    ...employee,
    age: 0,
    salary: 0
}

// const newEmployee = {
//     ...employee,
//     age: this.age,
//     salary: this.salary
// }

console.log(newEmployee);