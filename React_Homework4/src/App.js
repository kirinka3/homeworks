import React, { Component } from 'react';
import './App.scss';
import Main from './pages/Main';
import Header from './containers/Header';
import Cart from './pages/Cart'
import Favorites from './pages/Favorites'
import {
  Switch,
  Route
} from "react-router-dom";


class App extends Component {

  render() {
    return (
    <>
      <Header />
      <Switch>
        <Route exact path="/">
          <Main />
        </Route>
        <Route exact path="/cart">
          <Cart />
        </Route>
        <Route exact path="/favorites">
          <Favorites />
        </Route>
        </Switch>  
      </>
    )
  }
}

export default App;
