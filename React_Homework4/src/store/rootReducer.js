import * as types from './types'
import {combineReducers} from 'redux'
import cardReducer from "./card/cardReducer"
import modalReducer from './modal/modalReducer'

export default combineReducers({
    cardReducer,
    modalReducer
})


