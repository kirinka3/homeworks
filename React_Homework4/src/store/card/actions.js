import * as types from '../types'

const setCards = cards => ({
    type: types.SET_CARDS,
    payload: cards
})



const fetchCards = () => (dispatch) => {
    fetch('productsList.json')
        .then(response=> response.json())
        .then(data => dispatch(setCards(data)));
}

export default fetchCards