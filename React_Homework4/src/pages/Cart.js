import React, { useState, useEffect } from 'react';
import getPhones from '../api/getPhones';
import ProductList from '../components/ProductList';
import { useSelector, useDispatch} from 'react-redux'
import fetchCards from '../store/card/actions';

const Cart = () => {
  const dispatch = useDispatch()
  const phones = useSelector(store => store.cardReducer.phones)

  useEffect(() => {
    dispatch(fetchCards())
  },[dispatch])

  
  const [cartPhones, setCartPhones] = useState([]);

  useEffect(() => {
    const phoneArr = JSON.parse(localStorage.getItem('cart'));
    if (phoneArr) {
      setCartPhones(
        phones.filter(obj => phoneArr.find(id => id === obj.code))
      )
    }
  }, [phones])

  return (
    <div>
      <h1>Cart : </h1>
      <ProductList isInCart phonesList={cartPhones}/>
    </div>
  )
}

export default Cart