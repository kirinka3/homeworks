const ipBtn = document.querySelector('.btn')

ipBtn.addEventListener('click', function() {
    getIp();
})

async function getIp() {
    const resolve = await fetch('https://api.ipify.org/?format=json')
    const data = await resolve.json()
    const newResolve = await fetch (`http://ip-api.com/json/${data.ip}?fields=country,regionName,countryCode,city,timezone`)
    const newData = await newResolve.json()
    showElements(newData);
}

function showElements(newData) {
    const { city, country, countryCode, regionName, timezone } = newData;
        document.body.insertAdjacentHTML('beforeend', `
            <p>${city}</p>
            <p>${country}</p>
            <p>${countryCode}</p>
            <p>${regionName}</p> 
            <p>${timezone}</p>`)
}
