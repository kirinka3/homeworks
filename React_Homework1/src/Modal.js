import React, { Component } from 'react';

class Modal extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { header, text, isClose, actions, closeWindow } = this.props

        return (
                <div className="modal_overlay" onClick={closeWindow}>
                    <div className="modal_window">
                        <div className="modal_header">
                            <div className="modal_header_text">{header}</div>
                            {isClose && <div className="modal_close" onClick={closeWindow}>x</div>}
                        </div>
                        <div className="modal_body">{text}</div>
                        <div className="modal_footer">{actions}</div>
                    </div>
                </div>
        )
    }
}

export default Modal;