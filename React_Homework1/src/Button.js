import React, { Component } from 'react'

class Button extends Component {

    render() {
        const { backgroundColor, text, onClick} = this.props

        return (
            <button className={backgroundColor} onClick={onClick}>{text}</button>
        )
    }
}

export default Button;