import React, { Component } from 'react';
import Button from './Button';
import Modal from './Modal';
import './App.scss';

class App extends Component {
  state = {
    isFirstModalOpen: false,
    isSecondModalOpen: false,
  }

  showFirstModal = () => {
    this.setState({
      isFirstModalOpen: true
    })
  }

  showSecondModal = () => {
    this.setState({
      isSecondModalOpen: true
    })
  }

  closeModal = () => {
    if(this.state.isFirstModalOpen) {
      this.setState({
        isFirstModalOpen: false,
      })
    } else if (this.state.isSecondModalOpen) {
      this.setState({
        isSecondModalOpen: false,
      })
    }
  }

  confirmModal = () => {
    alert('Are you sure?')
  }


  render() {

    return (
      <div>
        {this.state.isFirstModalOpen && <Modal 
        header="Do you want to delete this file?" 
        text="Once you delete this file, it won't be possible to undo this action. 
        Are you sure you want to delete it?" 
        isClose
        actions={ 
          <>
            <Button backgroundColor="white" text="Ok" onClick={this.confirmModal} />
            <Button backgroundColor="white" text="Cancel" onClick={this.closeModal} />
          </>}
        closeWindow={this.closeModal}
        />}

        {this.state.isSecondModalOpen && <Modal 
        header="Want to load this file?" 
        text="Press Ok to upload this file" 
        isClose={false}
        actions={ 
          <>
            <Button backgroundColor="grey" text="YES" onClick={this.confirmModal} />
            <Button backgroundColor="grey" text="NO" onClick={this.closeModal} />
          </>}
          closeWindow={this.closeModal}
          />}
        
        <Button backgroundColor="green" text="Open first modal" onClick={this.showFirstModal} />
        <Button backgroundColor="yellow" text="Open second modal" onClick={this.showSecondModal} />
      </div>
    )
  }
}

export default App;
