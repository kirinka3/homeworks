1. Обьяснить своими словами разницу между обьявлением переменных через var, let и const.

Переменные var существуют до объявления, они равны undefined.
Переменные var, можно объявлять снова, назначая им новые значения.
Var имеет функциональную область видимости, а не блочную.
Переменная var – одна на все итерации цикла и видна даже после цикла.
Если переменная инициализирована с помощью var за пределами функции, то она назначается глобальному объекту. 

Переменная, объявленная через let, видна только в том блоке, где объявлена.
Переменная let видна только после объявления.
Используя let нельзя повторно объявить одну и ту же переменную.
Каждому повторению цикла соответствует своя независимая переменная let.

Значения переменных, объявленных с использованием ключевых слов var или let, могут быть перезаписаны.

Объявление const задаёт переменную, которую нельзя менять.
Const обладает блочной областью видимости.


2. Почему объявлять переменную через var считается плохим тоном?

Переменные var не имеют блочной области видимости.
Объявление переменных var производится в начале исполнения функции, вне зависимости от того, в каком месте функции реально находятся их объявления.
Эти особенности var, негативно влияют на код. 
Поэтому, после ввода в стандарт let и const, именно они являются основным способом объявления переменных.