"use strict";

const inp = document.querySelector('#inputPas');
const iBtn = document.querySelector('#faEye');
iBtn.addEventListener('click', function(event) {
	if (inp.getAttribute('type') == 'password') {
		inp.removeAttribute('type');
		inp.setAttribute('type', 'text');
		iBtn.className = 'fas fa-eye-slash icon-password';
	} else {
		inp.removeAttribute('type');
		inp.setAttribute('type', 'password');
		iBtn.className = 'fas fa-eye icon-password';
	}
})

const inp1 = document.querySelector('#inputPas1');
const iBtn1 = document.querySelector('#faEye1');
iBtn1.addEventListener('click', function(event) {
	if (inp1.getAttribute('type') == 'password') {
		inp1.removeAttribute('type');
		inp1.setAttribute('type', 'text');
		iBtn1.className = 'fas fa-eye-slash icon-password';
	} else {
		inp1.removeAttribute('type');
		inp1.setAttribute('type', 'password');
		iBtn1.className = 'fas fa-eye icon-password';
	}
})

const btnSubmit = document.querySelector('.btn');
btnSubmit.addEventListener('click', function(event) {
	event.preventDefault();
	if (inp.value === inp1.value) {
		alert('You are welcome');
	} else {
		const newInp = document.createElement('p');
		newInp.innerHTML = 'Нужно ввести одинаковые значения';
		newInp.style.color = 'red';
		inp1.after(newInp);
	}
})

