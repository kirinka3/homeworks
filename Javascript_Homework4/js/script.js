"use strict";

function createNewUser() {
  let firstName = prompt('Enter your first name');
  let lastName = prompt('Enter your last name');

  const newUser = {
    firstName: firstName,
    lastName: lastName,
    getLogin: function() {
      return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
    },
  }
  return newUser;
}

let user = createNewUser();

console.log(user.getLogin());