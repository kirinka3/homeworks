"use strict";

const books = [
  { 
    author: "Скотт Бэккер",
    name: "Тьма, что приходит прежде",
    price: 70 
  }, 
  {
   author: "Скотт Бэккер",
   name: "Воин-пророк",
  }, 
  { 
    name: "Тысячекратная мысль",
    price: 70
  }, 
  { 
    author: "Скотт Бэккер",
    name: "Нечестивый Консульт",
    price: 70
  }, 
  {
   author: "Дарья Донцова",
   name: "Детектив на диете",
   price: 40
  },
  {
   author: "Дарья Донцова",
   name: "Дед Снегур и Морозочка",
  }
];


const divEl = document.getElementById('root');
const ul = document.createElement('ul');

const newBooks = books.map(item => {
  try {
  return validateBook(item);
  } catch(err) {
  console.error(err);
  }
})

function validateBook(item) {
    if(!item.hasOwnProperty('author')) {
      throw new Error('no author');
  } else if (!item.hasOwnProperty('name')) {
      throw new Error('no name');
  } else if (!item.hasOwnProperty('price')) {
      throw new Error('no price');
  } else {
      return (`<li>
      <p>${item.author}</p>
      <p>${item.name}</p>
      <p>Цена ${item.price}</p>
      </li>`);
  }
}

ul.insertAdjacentHTML("afterbegin", newBooks.join(' '))
divEl.append(ul);