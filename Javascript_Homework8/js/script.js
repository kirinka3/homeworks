"use strict";

const inputEl = document.querySelector('#input-in');
const btnEl = document.querySelector("#btn");

inputEl.addEventListener('mouseenter', function () {
    inputEl.style.borderColor = 'green';
})
inputEl.addEventListener('mouseleave', function () {
    inputEl.style.borderColor = '';
})

inputEl.addEventListener('blur', function() {
    inputEl.style.borderColor = '';
    const priceEl = +inputEl.value;
    inputEl.innerText = priceEl;
    if (priceEl < 0) {
        inputEl.style.borderColor = 'red';
        const par = document.createElement('p');
        par.innerHTML = 'Please enter correct price';
        const btnEl = document.querySelector("#btn");
        btnEl.after(par);  
    }
    else {
        const parEl = document.createElement('div');
        const spanEl = document.createElement('span');
        const btnNo = document.createElement('button');
        spanEl.innerHTML = `Current price: ${priceEl}$`;
        btnNo.innerHTML = 'x';
        inputEl.before(parEl);
        parEl.appendChild(spanEl);
        parEl.appendChild(btnNo);
        inputEl.style.backgroundColor = '#7bdf7b';
    
        btnNo.addEventListener('click', function(event) {
            parEl.remove();
            inputEl.value = '';
            inputEl.style.backgroundColor = '';
            });
    }
    
})
  
