"use strict";

const tabsBtn   = document.querySelectorAll(".tabs-nav-btn");
const tabsItems = document.querySelectorAll(".tabs__item");
tabsBtn.forEach(onTabClick);
function onTabClick(item) {
    item.addEventListener("click", function() {
        let currentBtn = item;
        let tabId = currentBtn.getAttribute("data-tab");
        let currentTab = document.querySelector(tabId);
        if( ! currentBtn.classList.contains('active') ) {
            tabsBtn.forEach(function(item) {
                item.classList.remove('active');
            });    
            tabsItems.forEach(function(item) {
                item.classList.remove('active');
            });   
            currentBtn.classList.add('active');
            currentTab.classList.add('active');
        }
    });
}
document.querySelector('.tabs-nav-btn').click();



const tabWork = $('ul.filter-menu > li');
tabWork.click (function( event ) {
    tabWork.removeClass('active');
    $(this).addClass('active');
    const targetFilter = $(this).data('target');
    const targetFilterImg = $('.filter-images .work-container');
    if(targetFilter == 'all') {
        targetFilterImg.show();
    } else {
        targetFilterImg.hide();
        targetFilterImg.filter('.' +targetFilter).show();
    }
});

const btnLoad = $(".btn-load");
btnLoad.click(function( event ) {
    $('.hide').removeClass('hide');
    $(this).remove();
})



const sliderImg = $('div.slider-container .slider-content')
$('button.next').click(function (event) {
    const current = $('.slider-container').data('current');
    $(sliderImg[current]).hide();
    let next = current + 1;
    if (next >= sliderImg.length) {
        next = 0;
    }
    $(sliderImg[next]).show();
    $('.slider-container').data('current', next);
})

$('button.prev').click(function (event) {
    const current = $('.slider-container').data('current');
    $(sliderImg[current]).hide();
    let next = current - 1;
    if (next < 0) {
        next = sliderImg.length - 1;
    }
    $(sliderImg[next]).show();
    $('.slider-container').data('current', next);
})

const imgSmall = $('.slider-row img')
imgSmall.click(function (event) {
    const num = $(this).parent().data('number');
    console.log(num);
    const current = $('.slider-container').data('current');
    $(sliderImg[current]).hide(500);
    $(sliderImg[num]).show(1000);
    $('.slider-container').data('current', num);
})
