"use strict";

function createNewUser() {
  const firstName = prompt('Enter your first name');
  const lastName = prompt('Enter your last name');
  const birthday = prompt('Enter your birthday, dd.mm.yyyy');

  const newUser = {
    firstName: firstName,
    lastName: lastName,
    getLogin: function() {
      return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
    },
    getAge: function() {
      let nowDate = new Date();
      let userDate = Date.parse(birthday.split('.').reverse().join('.'));
      let age = Math.floor((nowDate - userDate)/(24 * 3600 * 365.25 * 1000));
      return age;
    },
    getPassword: function() {
      return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + birthday.substring(6);
    },
  }
  return newUser;
}

const user = createNewUser();

console.log(user.getLogin());
console.log(user.getAge());
console.log(user.getPassword());