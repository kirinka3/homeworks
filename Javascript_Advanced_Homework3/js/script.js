class Employee {
    constructor(name, age, salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }

    set eName(name) {
        this.name = name;
    }

    get eName() {
        return this.name;
    }

    set eAge(age) {
        this.age = age;
    }

    get eAge() {
        return this.age;
    }

    set eSalary(salary) {
        this.salary = salary;
    }

    get eSalary() {
        return this.salary;
    }

}

const emp = new Employee('имя', 'возраст', 'зарплата');
console.log(emp);


class Programmer extends Employee {
    constructor(name, age, salary, lang=[]) {
        super(name, age, salary);
        this.lang = lang;
    }

    get eSalary() {
        return this.salary * 3;
    }

}

const egor = new Programmer('Egor', 13, 2000, ['javascript']);
const andrew = new Programmer('Andrew', 38, 3000, ['php', 'javascript']);
const bogdan = new Programmer('Bogdan', 27, 5000, ['php', 'java']);

console.log(egor);
console.log(andrew);
console.log(bogdan);