import React from 'react';
import PropTypes from 'prop-types';

const Modal = (props) => {

    const { header, text, isClose, actions, closeWindow } = props;

        return (
            <div className="modal_overlay" onClick={closeWindow}>
                <div className="modal_window">
                    <div className="modal_header">
                        <div className="modal_header_text">{header}</div>
                        {isClose && <div className="modal_close" onClick={closeWindow}>x</div>}
                    </div>
                    <div className="modal_body">{text}</div>
                    <div className="modal_footer">{actions}</div>
                </div>
            </div>
        )
}

Modal.propTypes = {
    header: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired,
    isClose: PropTypes.bool.isRequired,
    actions: PropTypes.any.isRequired,
    closeWindow: PropTypes.func.isRequired
};


export default Modal;