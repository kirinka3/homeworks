import React, { useState } from 'react';
import Modal from '../components/Modal';
import Button from '../components/Button';
import Product from '../components/Product';
import PropTypes from 'prop-types'

const ProductList = (props) => {
    const [currentId, setCurrentId] = useState(null);
    const [modalIsOpened, setModalIsOpened] = useState(false);

    const showModal = () => {
        setModalIsOpened(true)
    }
    
    const closeModal = () => {
        if(modalIsOpened) {
        setModalIsOpened(false)
        }
    }

    const addToFavorite = (id) => {
        const favArray = localStorage.getItem('favorite')

        if(favArray) {
            const parsedArr = JSON.parse(favArray)
            const indexFav = parsedArr.indexOf(id)

            if (indexFav === -1) {
                parsedArr.push(id)
                localStorage.setItem('favorite', JSON.stringify(parsedArr))
            } else {
                const spliced = parsedArr.splice(indexFav, 1)
                localStorage.setItem('favorite',JSON.stringify(parsedArr))
            }
        } else {
            localStorage.setItem('favorite',JSON.stringify([id]))
        }
    }

    const addToCart = () => {
        const cartArr = localStorage.getItem('cart')
        if(cartArr) {
            const parsedArr = JSON.parse(cartArr)
            const indexCart = parsedArr.indexOf(currentId)
            if (indexCart === -1) {
                parsedArr.push(currentId)
                localStorage.setItem('cart', JSON.stringify(parsedArr))
            }
        } else {
            localStorage.setItem('cart',JSON.stringify([currentId]))
        }  
    }

    const removeFromCart = (id) => {
        const cartArray = localStorage.getItem('cart')
        if(cartArray) {
            const parsedArr = JSON.parse(cartArray)
            const indexCart = parsedArr.indexOf(id)
            parsedArr.splice(indexCart, 1)
            localStorage.setItem('cart', JSON.stringify(parsedArr))
        } 
    }

    const getId = id => {
        setCurrentId(id)
        showModal()
    }

    const phonesArr = props.phonesList.map((item) => {
        return (
            <Product 
                key={item.code}
                code={item.code}
                product={item.product}
                price={item.price}
                color={item.color} 
                url={item.url}
                onClickFavorite={() => addToFavorite(item.code)}
                onClickAddToCard={() => getId(item.code)}
                isInCart={props.isInCart}
                showModal={showModal}
                />
            )
        })

        return (
            <>
            <div className="card_list">{phonesArr}</div>
        
            {modalIsOpened && <Modal 
                header="" 
                text="You have added item to cart" 
                isClose
                actions={ 
                <>
                    {/* <Button backgroundColor="grey" text="OK" onClick={addToCart} /> */}
                    <Button backgroundColor="grey" text='Ok' onClick={() => {
                        props.isInCart ? removeFromCart(currentId) : addToCart(currentId)
                    }}></Button>
                    <Button backgroundColor="grey" text="CLOSE" onClick={closeModal} />
                </>}
                closeWindow={closeModal}
                />}
            </>
        )

}

ProductList.propTypes = {
    clothes: PropTypes.array
};

export default ProductList;