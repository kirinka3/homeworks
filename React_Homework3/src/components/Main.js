import React from 'react';
import getPhones from '../api/getPhones';
import ProductList from '../components/ProductList';

const Main = () => {
  const phones = getPhones()

  return (
    <ProductList phonesList={phones}/>
  )
}

export default Main;