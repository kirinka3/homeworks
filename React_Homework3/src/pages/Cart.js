import React, { useState, useEffect } from 'react';
import getPhones from '../api/getPhones';
import ProductList from '../components/ProductList';

const Cart = () => {
  const phones = getPhones();
  const [cartPhones, setCartPhones] = useState([]);

  useEffect(() => {
    const phoneArr = JSON.parse(localStorage.getItem('cart'));
    if (phoneArr) {
      setCartPhones(
        phones.filter(obj => phoneArr.find(id => id === obj.code))
      )
    }
  }, [phones])

  return (
    <div>
      <h1>Cart : </h1>
      <ProductList isInCart phonesList={cartPhones}/>
    </div>
  )
}

export default Cart