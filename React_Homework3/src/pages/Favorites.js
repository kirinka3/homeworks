import React, { useState, useEffect } from 'react';
import getPhones from '../api/getPhones';
import ProductList from '../components/ProductList';

const Favorites = () => {

    const phones = getPhones();

    const [favoritePhones, setFavoritePhones] = useState([])

    useEffect(() => {
        const phoneArr = JSON.parse(localStorage.getItem('favorite'));

        if (phoneArr) {
            setFavoritePhones(
                phones.filter(obj => phoneArr.find(id => id === obj.code))
            )
        }
            }, [phones])

    return (
        <div>
            <h1>Favorites : </h1>
            <ProductList phonesList={favoritePhones}/>
        </div>
    )
}

export default Favorites