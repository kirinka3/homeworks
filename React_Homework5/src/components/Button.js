import React from 'react';
import PropTypes from 'prop-types'

const Button = (props) => {
    const { backgroundColor, text, onClick} = props;

        return (
            <button className={backgroundColor} onClick={onClick}>{text}</button>
        )
}

Button.propTypes={
    backgroundColor: PropTypes.string,
    text: PropTypes.string.isRequired,
    onClick: PropTypes.func
};

export default Button;