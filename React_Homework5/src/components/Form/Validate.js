import * as yup from 'yup'

const phoneValidate = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/

const Validate = yup.object().shape({
    firstName: yup
    .string()
    .min(2, 'Minimum name length is 2 symbols')
    .required('This field is required'),
    lastName: yup
    .string()
    .min(2, 'Minimum name length is 2 symbols')
    .required('This field is required'),
    age: yup
    .number()
    .required('This field is required'),
    phoneNumber: yup
    .string()
    .matches(phoneValidate, 'Enter correct number')
    .required('This field is required'),
    address: yup
    .string()
    .min(5, 'Minimum name length is 20 symbols')
    .required('This field is required'),
});

export default Validate