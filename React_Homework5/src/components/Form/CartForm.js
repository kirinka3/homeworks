import React from 'react'
import {useDispatch} from 'react-redux'
import {setForm, clearStorage} from '../../store/form/actions'
import { Formik } from 'formik'
import Validate from './Validate'


const CartForm = () => {
    const dispatch = useDispatch()

    return(
    <div className="cart_form">
        <h2 className="title_form">Fill the form</h2>
        <Formik
        initialValues={{ 
            firstName: '',
            lastName: '',
            age: '',
            address: '',
            phoneNumber: '', 
        }}

        validationSchema={Validate}

        onSubmit={(values) => {
            dispatch(setForm(values));
            dispatch(clearStorage());
        }}
        >
        {({
            values,
            errors,
            touched,
            handleChange,
            handleBlur,
            handleSubmit,
            isSubmitting,
        }) => {
        return(
        <form onSubmit={handleSubmit}>
            <input className="form_field" 
                placeholder="First name"
                type="text"
                name="firstName"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.firstName}
            />
            {errors.firstName && touched.firstName && errors.firstName}
            <input className="form_field" 
                placeholder="Last name"
                type="text"
                name="lastName"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.lastName}
            />
            {errors.lastName && touched.lastName && errors.lastName}
            <input className="form_field" 
                placeholder="Age"
                type="number"
                name="age"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.age}
            />
            {errors.age && touched.age && errors.age}
            <input className="form_field" 
                placeholder="Address"
                type="text"
                name="address"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.address}
            />
            {errors.address && touched.address && errors.address}
            <input className="form_field" 
                placeholder="Phone number"
                type="text"
                name="phoneNumber"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.phoneNumber}
            />
            {errors.phoneNumber && touched.phoneNumber && errors.phoneNumber}
            <button className="form_btn" type="submit">
                Checkout
            </button>
            </form>
        
        )}}
        </Formik>
    </div>
)};

export default CartForm;
