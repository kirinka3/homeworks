import * as types from '../types'

export const setForm = values => ({
    type: types.SET_FORM_DATA,
    payload: values
})

export const clearStorage = () => ({
    type: types.CLEAR_STORAGE
})
