import * as types from '../types'

export const openModal = () => ({
    type: types.SHOW_MODAL,
    payload: true
})

export const clearModal = () => ({
    type: types.CLOSE_MODAL,
    payload: false
})
