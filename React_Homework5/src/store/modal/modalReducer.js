import * as types from '../types'

const initialState = {
    modalIsOpened: false
};

const modalReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.SHOW_MODAL:
            return {
                modalIsOpened: action.payload
            }
        case types.CLOSE_MODAL:
            return {
                modalIsOpened: action.payload
            }
        default: 
        return state
    }
}


export default modalReducer